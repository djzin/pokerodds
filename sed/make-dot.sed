#!/usr/bin/sed -f

1i digraph {

s/^\(....\) \(....\)/    "\1" -> "\2"/

$a }
