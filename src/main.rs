mod card;
mod classify;

use std::sync::{Arc, Mutex};
use std::thread;

use card::*;
use classify::*;

use card::Suit::*;

fn run_for_card(mutex: Arc<Mutex<()>>, card_index: usize) {
    let cards = FIFTY_TWO_CARD_DECK;
    let i = card_index;
    for j in i+1..52 {
        if cards[j].suit == Diamonds || cards[j].suit == Clubs { continue; }
        for k in 0..52 {
            if cards[k].suit == Clubs { continue; }
            if cards[j].suit == Spades && cards[k].suit == Diamonds { continue; }
            for l in k+1..52 {
                if (cards[j].suit == Spades || cards[k].suit == Spades) && cards[l].suit == Clubs { continue; }
                if cards[j].suit == Hearts && cards[k].suit == Hearts && cards[l].suit == Clubs { continue; }
                if cards[j].suit == Spades && cards[k].suit == Spades && cards[l].suit == Diamonds { continue; }
                if k == i || k == j || l == i || l == j { continue; }
                let player1 = [cards[i], cards[j]];
                let player2 = [cards[k], cards[l]];
                let odds = heads_up_odds(player1, player2);
                let _lock = mutex.lock();
                println!("{}{} {}{} - Favourite: {} - {} - {:?}", player1[0], player1[1], player2[0], player2[1], odds.favourite(), odds, odds);
            }
        }
    }
}

fn main() {
    let mut threads = Vec::new();
    let mutex = Arc::new(Mutex::new(()));
    for &i in &[0, 4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48] {
        let mutex = mutex.clone();
        threads.push(thread::spawn(move || run_for_card(mutex, i)));
    }
    for thread in threads {
        thread.join().unwrap();
    }
}
