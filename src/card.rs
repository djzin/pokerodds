#![allow(non_upper_case_globals)]

use std::fmt::{self, Display, Formatter};

use self::Rank::*;
use self::Suit::*;

#[derive(Copy, Clone, PartialOrd, Ord, PartialEq, Eq, Debug)]
pub enum Rank {
    Two = 2,
    Three = 3,
    Four = 4,
    Five = 5,
    Six = 6,
    Seven = 7,
    Eight = 8,
    Nine = 9,
    Ten = 10,
    Jack = 11,
    Queen = 12,
    King = 13,
    Ace = 14,
}

impl Display for Rank {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        f.write_str(match *self {
            Two => "2",
            Three => "3",
            Four => "4",
            Five => "5",
            Six => "6",
            Seven => "7",
            Eight => "8",
            Nine => "9",
            Ten => "T",
            Jack => "J",
            Queen => "Q",
            King => "K",
            Ace => "A",
        })
    }
}

#[derive(Copy, Clone, PartialOrd, Ord, PartialEq, Eq, Debug)]
pub enum Suit {
    Clubs,
    Diamonds,
    Hearts,
    Spades,
}

impl Display for Suit {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        f.write_str(match *self {
            Clubs => "♣",
            Diamonds => "♢",
            Hearts => "♡",
            Spades => "♠",
        })
    }
}

#[derive(Copy, Clone, PartialOrd, Ord, PartialEq, Eq, Debug)]
pub struct Card {
    pub rank: Rank,
    pub suit: Suit,
}

impl Display for Card {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}{}", self.rank, self.suit)
    }
}

pub const TwoOfClubs: Card = Card{rank: Two, suit: Clubs};
pub const ThreeOfClubs: Card = Card{rank: Three, suit: Clubs};
pub const FourOfClubs: Card = Card{rank: Four, suit: Clubs};
pub const FiveOfClubs: Card = Card{rank: Five, suit: Clubs};
pub const SixOfClubs: Card = Card{rank: Six, suit: Clubs};
pub const SevenOfClubs: Card = Card{rank: Seven, suit: Clubs};
pub const EightOfClubs: Card = Card{rank: Eight, suit: Clubs};
pub const NineOfClubs: Card = Card{rank: Nine, suit: Clubs};
pub const TenOfClubs: Card = Card{rank: Ten, suit: Clubs};
pub const JackOfClubs: Card = Card{rank: Jack, suit: Clubs};
pub const QueenOfClubs: Card = Card{rank: Queen, suit: Clubs};
pub const KingOfClubs: Card = Card{rank: King, suit: Clubs};
pub const AceOfClubs: Card = Card{rank: Ace, suit: Clubs};

pub const TwoOfDiamonds: Card = Card{rank: Two, suit: Diamonds};
pub const ThreeOfDiamonds: Card = Card{rank: Three, suit: Diamonds};
pub const FourOfDiamonds: Card = Card{rank: Four, suit: Diamonds};
pub const FiveOfDiamonds: Card = Card{rank: Five, suit: Diamonds};
pub const SixOfDiamonds: Card = Card{rank: Six, suit: Diamonds};
pub const SevenOfDiamonds: Card = Card{rank: Seven, suit: Diamonds};
pub const EightOfDiamonds: Card = Card{rank: Eight, suit: Diamonds};
pub const NineOfDiamonds: Card = Card{rank: Nine, suit: Diamonds};
pub const TenOfDiamonds: Card = Card{rank: Ten, suit: Diamonds};
pub const JackOfDiamonds: Card = Card{rank: Jack, suit: Diamonds};
pub const QueenOfDiamonds: Card = Card{rank: Queen, suit: Diamonds};
pub const KingOfDiamonds: Card = Card{rank: King, suit: Diamonds};
pub const AceOfDiamonds: Card = Card{rank: Ace, suit: Diamonds};

pub const TwoOfHearts: Card = Card{rank: Two, suit: Hearts};
pub const ThreeOfHearts: Card = Card{rank: Three, suit: Hearts};
pub const FourOfHearts: Card = Card{rank: Four, suit: Hearts};
pub const FiveOfHearts: Card = Card{rank: Five, suit: Hearts};
pub const SixOfHearts: Card = Card{rank: Six, suit: Hearts};
pub const SevenOfHearts: Card = Card{rank: Seven, suit: Hearts};
pub const EightOfHearts: Card = Card{rank: Eight, suit: Hearts};
pub const NineOfHearts: Card = Card{rank: Nine, suit: Hearts};
pub const TenOfHearts: Card = Card{rank: Ten, suit: Hearts};
pub const JackOfHearts: Card = Card{rank: Jack, suit: Hearts};
pub const QueenOfHearts: Card = Card{rank: Queen, suit: Hearts};
pub const KingOfHearts: Card = Card{rank: King, suit: Hearts};
pub const AceOfHearts: Card = Card{rank: Ace, suit: Hearts};

pub const TwoOfSpades: Card = Card{rank: Two, suit: Spades};
pub const ThreeOfSpades: Card = Card{rank: Three, suit: Spades};
pub const FourOfSpades: Card = Card{rank: Four, suit: Spades};
pub const FiveOfSpades: Card = Card{rank: Five, suit: Spades};
pub const SixOfSpades: Card = Card{rank: Six, suit: Spades};
pub const SevenOfSpades: Card = Card{rank: Seven, suit: Spades};
pub const EightOfSpades: Card = Card{rank: Eight, suit: Spades};
pub const NineOfSpades: Card = Card{rank: Nine, suit: Spades};
pub const TenOfSpades: Card = Card{rank: Ten, suit: Spades};
pub const JackOfSpades: Card = Card{rank: Jack, suit: Spades};
pub const QueenOfSpades: Card = Card{rank: Queen, suit: Spades};
pub const KingOfSpades: Card = Card{rank: King, suit: Spades};
pub const AceOfSpades: Card = Card{rank: Ace, suit: Spades};

pub const FIFTY_TWO_CARD_DECK: [Card; 52] = [
    AceOfSpades,
    AceOfHearts,
    AceOfDiamonds,
    AceOfClubs,
    KingOfSpades,
    KingOfHearts,
    KingOfDiamonds,
    KingOfClubs,
    QueenOfSpades,
    QueenOfHearts,
    QueenOfDiamonds,
    QueenOfClubs,
    JackOfSpades,
    JackOfHearts,
    JackOfDiamonds,
    JackOfClubs,
    TenOfSpades,
    TenOfHearts,
    TenOfDiamonds,
    TenOfClubs,
    NineOfSpades,
    NineOfHearts,
    NineOfDiamonds,
    NineOfClubs,
    EightOfSpades,
    EightOfHearts,
    EightOfDiamonds,
    EightOfClubs,
    SevenOfSpades,
    SevenOfHearts,
    SevenOfDiamonds,
    SevenOfClubs,
    SixOfSpades,
    SixOfHearts,
    SixOfDiamonds,
    SixOfClubs,
    FiveOfSpades,
    FiveOfHearts,
    FiveOfDiamonds,
    FiveOfClubs,
    FourOfSpades,
    FourOfHearts,
    FourOfDiamonds,
    FourOfClubs,
    ThreeOfSpades,
    ThreeOfHearts,
    ThreeOfDiamonds,
    ThreeOfClubs,
    TwoOfSpades,
    TwoOfHearts,
    TwoOfDiamonds,
    TwoOfClubs,
];
