Poker Odds & Topology
=====================

This project computes poker odds for heads-up Texas Hold'em.
It is written in rust, sed and gvpr.

To compute a pretty PDF `out/hands.pdf` of the topology of winning poker hands, simply type:

```
make
```

This can take a couple of hours to generate the odds file `out/hands`, then a short time to generate the PDF from the hand probabilities.

Dependencies
------------

* rust
* sed
* graphviz

To run tests, type:

```
make check
```

The tests have an additional dependency on rust's `rand` library.