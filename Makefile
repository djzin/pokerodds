SORT_AND_DEDUP = tr TJQKA AJKQT | sort -u | tr AJKQT TJQKA

default: out/hands.pdf

out:
	mkdir -p out

out/hands: out $(wildcard src/*.rs)
	cargo run --release > $@

out/hands.all: sed/permutations.sed out/hands
	$^ > $@

out/hands.all.dedup: sed/reorder-pairs.sed out/hands.all
	$^ | $(SORT_AND_DEDUP) > $@

out/hands.all.simplified: sed/simplify-suits.sed out/hands.all
	$^ | $(SORT_AND_DEDUP) > $@

out/hands.all.dedup.p1 out/hands.all.simplified.p1: out/%.p1: sed/player1.sed out/%
	$^ | $(SORT_AND_DEDUP) > $@

out/hands.all.dedup.p1.dot out/hands.all.simplified.p1.dot: out/%.dot: sed/make-dot.sed out/%
	$^ > $@

out/hands.all.dedup.p1.dot.sccmap out/hands.all.simplified.p1.dot.sccmap: out/%.sccmap: out/%
	sccmap -d $^ > $@

out/hands.all.dedup.p1.dot.sccmap.combined out/hands.all.simplified.p1.dot.sccmap.combined: out/%.combined: gvpr/combine-sccmap.gvpr out/%
	$^ > $@

out/hands.all.dedup.p1.dot.sccmap.middle out/hands.all.simplified.p1.dot.sccmap.middle: out/%.middle: gvpr/extract-middle.gvpr out/%
	$^ > $@

out/hands.all.dedup.p1.dot.sccmap.middle.tred out/hands.all.dedup.p1.dot.sccmap.combined.tred out/hands.all.simplified.p1.dot.sccmap.middle.tred out/hands.all.simplified.p1.dot.sccmap.combined.tred: out/%.tred: out/%
	tred $^ > $@

out/hands.all.dedup.p1.dot.sccmap.middle.tred.together out/hands.all.simplified.p1.dot.sccmap.middle.tred.together: out/%.together: gvpr/combine-parts.gvpr out/%
	$^ > $@

out/hands.dedup.final out/hands.simplified.final: out/hands.%.final: out/hands.all.%.p1.dot.sccmap.combined.tred out/hands.all.%.p1.dot.sccmap.middle.tred.together
	gvpack -u $^ > $@

out/hands.pdf: out/hands.simplified.final
	dot -Tpdf $^ > $@

check: test
test:
	cargo test

clean:
	rm -rf out

.PHONY: default check test clean
